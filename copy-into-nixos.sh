#!/usr/bin/env bash
# su -c - nix 'cd rain-nixos-configuration/ && git pull'
su -c - nix 'cd rain-nixos-configuration/ ; /home/nix/Programming/bin/rm-emacs'
su -c - nix 'cd rain-nixos-configuration/conf ; /home/nix/Programming/bin/rm-emacs'
cp -r rain-nixos-configuration/* /etc/nixos/
chown -R root /etc/nixos
nixos-rebuild build
echo nixos-rebuild switch
