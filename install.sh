#!/usr/bin/env bash

function install_file() {
    mkdir -p "$2"
    cp "$1" "$2"
}

install_file \
    ffmpegthumbnailer.thumbnailer \
    ~/.local/share/thumbnailers/
install_file \
    mupdf.desktop \
    ~/.local/share/applications/

update-desktop-database ~/.local/share/applications
